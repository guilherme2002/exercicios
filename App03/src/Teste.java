/**
 * 
 * @author 2017101257
 *
 */
public class Teste {
	
	public static int dobra(int num) {
		return num*2;
	}
	
	public static void main(String[] args) {
		int numero = 2112;
		System.out.println("O valor de numero �: " + numero);
		System.out.println("Dobrando seu valor.");
		numero = dobra(numero);
		System.out.println("Afora o valor do n�mero �: " + numero);
		
	}
	
	

}
